#!/usr/bin/env bash
# Original code from https://github.com/MunifTanjim/tmux-mode-indicator/blob/master/mode_indicator.tmux
# Author: Munif Tanjim
# Email: github@muniftanjim.dev
# MIT Licensed, available at https://github.com/MunifTanjim/tmux-mode-indicator
# Copied from original to modify the mode indicators

set -e

declare -r mode_indicator_placeholder="\#{tmux_mode_indicator}"

tmux_option() {
  local -r option=$(tmux show-option -gqv "$1")
  local -r fallback="$2"
  echo "${option:-$fallback}"
}

indicator_style() {
  local -r style="bg=$1,fg=#fafafa"
  echo "${style:+#[${style//,/]#[}]}"
}

reverse_indicator_style() {
  local -r style="bg=#fafafa,fg=$1"
  echo "${style:+#[${style//,/]#[}]}"
}

mode_selector() {
  echo "#{?client_prefix,$1,#{?pane_in_mode,$2,#{?pane_synchronized,$3,$4}}}"
}

init_tmux_mode_indicator() {
  local -r \
    prefix_prompt="wait #[bg=#e5e5e6] " \
    copy_prompt="copy #[bg=#e5e5e6] " \
    sync_prompt="sync #[bg=#e5e5e6] " \
    empty_prompt="%H:%M #[bg=#e5e5e6] " \
    prefix_style=$(indicator_style "green") \
    copy_style=$(indicator_style "yellow") \
    sync_style=$(indicator_style "red") \
    empty_style=$(indicator_style "#61afef")
    prefix_lhs_style=$(reverse_indicator_style "green") \
    copy_lhs_style=$(reverse_indicator_style "yellow") \
    sync_lhs_style=$(reverse_indicator_style "red") \
    empty_lhs_style=$(reverse_indicator_style "#61afef")

  local -r \
    mode_prompt=$(mode_selector "$prefix_prompt" "$copy_prompt" "$sync_prompt" "$empty_prompt")\
    mode_text_style=$(mode_selector $prefix_style $copy_style $sync_style $empty_style)\
    mode_lhs_style=$(mode_selector $prefix_lhs_style $copy_lhs_style $sync_lhs_style $empty_lhs_style)

  local -r mode_indicator="$mode_lhs_style $mode_text_style$mode_prompt#[default]"

  local -r status_right_value="$(tmux_option "status-right")"
  tmux set-option -gq "status-right" "${status_right_value/$mode_indicator_placeholder/${mode_indicator}}"
}

init_tmux_mode_indicator
