#!/bin/bash
# Creates the status bar used in the tmux dotline configuration

cpu_usage() {
  cpu_val=$(echo "$(cat /proc/loadavg | cut -f 1 -d ' ') / $(nproc)" | bc -l | cut -c -4)
  printf "%.2f" "$cpu_val"
}

cpu_usage

