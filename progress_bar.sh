#!/bin/bash
# Show a unicode block character corresponding to current usage rates.
# If the percentage is > 100%, show a dot. If it is greater than 400% (max # of dots), then 
# just show the percentage.

dots=(‧ : ⁝ ⁞)
bars=(▁ ▂ ▃ ▄ ▅ ▆ ▇ █)

read percent 
hundreds=${percent%.*}
if ((hundreds > 0)) && ((hundreds <= 4));
then
  printf "%s" ${dots[$((hundreds - 1))]}
elif ((hundreds > 4));
then
  printf "%.0f%%" $( echo "$percent * 100" | bc -l )
  exit 1
fi

if (( $(echo "$percent <= 0" | bc -l) ));
then
  printf " "
else
  index=$(echo "scale=0; ($percent / .125) % 8" | bc -l)
  printf "%s" ${bars[$index]}
fi
