#!/bin/bash
# Creates the status bar used in the tmux dotline configuration

cpu_display() {
  echo "$(cat /proc/loadavg | cut -f 2 -d ' ') / $(nproc)" | bc -l | cut -c -4
}

init_dotline() {
  dotline_dir="$(dirname "$(realpath "$0")")"

  tmux set -g status on

  # Update the status line every second
  tmux set -g status-interval 1

  # Set the position of window lists
  tmux set -g status-justify left # [left | centre | right]

  # Set status bar position
  tmux set -g status-position top # [top, bottom]

  # Set status bar background and foreground color
  tmux set -g status-style fg="#494b53",bg="#fafafa"

  # Status left is used for windows
  tmux set-window-option -g status-left ""
  tmux set-window-option -g status-left-length 0

  tmux set-window-option -g status-right-length 400

  local \
    inactive_win_lhs="#[fg=#e5e5e6,bg=#fafafa]"\
    inactive_win_body="#[fg=#2a2b33,bg=#e5e5e6]#W"\
    inactive_win_rhs="#[fg=#e5e5e6,bg=#fafafa]"
  # Inactive windows
  tmux set-window-option -g window-status-format "$inactive_win_lhs$inactive_win_body$inactive_win_rhs"

  # Active window
  local \
    cur_win_lhs="#[fg=#0184bc,bg=#fafafa]"\
    cur_win_body="#[fg=#fafafa,bg=#0184bc]#W"\
    cur_win_rhs="#[fg=#0184bc,bg=#fafafa]"

  tmux set-window-option -g window-status-current-format "$cur_win_lhs$cur_win_body$cur_win_rhs"

  # Status right
  local \
    cpu_bar="#( $dotline_dir/cpu.sh | $dotline_dir/progress_bar.sh )"\
    cpu_usage="#[fg=#50a14f,bg=#fafafa]  "\
    mem_bar="#( $dotline_dir/memory.sh | $dotline_dir/progress_bar.sh )"\
    mem_usage="#[fg=#c678dd,bg=#fafafa]  "\
    disk_bar="#( $dotline_dir/disk.sh | $dotline_dir/progress_bar.sh )"\
    disk_usage="#[fg=#c18401,bg=#fafafa]  "\
    current_path="#[fg=#a626a4,bg=#fafafa]  #{b:pane_current_path}"\
    hostname="#[fg=#2a2b33,bg=#e5e5e6]#H"\
    window_info="[#I]#F"\
    right_prompt_rhs="#[fg=#e5e5e6,bg=#fafafa]"

  tmux set-window-option -g status-right "$current_path $cpu_usage$cpu_bar $mem_usage$mem_bar $disk_usage$disk_bar #{tmux_mode_indicator}$hostname $window_info$right_prompt_rhs"
}

init_dotline
