#!/bin/bash
# Calculate percentage of memory used.
# Taken from https://dev.to/brandonwallace/make-your-tmux-status-bar-100-better-with-bash-2fne

# Display used, total, and percentage of memory using the free command.
read total used <<< $(free -m | awk '/Mem/{printf $2" "$3}')
# Calculate the percentage of memory used with bc.
percent=$(bc -l <<< "$used / $total")
echo $percent
