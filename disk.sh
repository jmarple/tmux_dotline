#!/bin/bash
df / -h --output=pcent | sed -n 2p | sed -e s/%// -e s/\ // | awk '{print "." $0}'
